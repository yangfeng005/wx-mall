var util = require('../../utils/util.js');
var api = require('../../config/api.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    images: [],
    fileUploadUrl: api.FileUploadUrl,
    fileDownloadUrl: api.FileDownloadUrl,
    min: 5,//最少字数
    max: 300, //最多字数 
    content: '',
    typeId: '',
    valueId: '',
    orderId: ''
  },

  onLoad: function (options) {
    var that = this;
    that.setData({
      typeId: options.typeId,
      valueId: options.valueId,
      orderId: options.orderId
    });
  },

  //字数限制  
  inputCheck: function (e) {
    // 获取输入框的内容
    var value = e.detail.value;
    this.setData({
      content: value
    });
    // 获取输入框内容的长度
    var len = parseInt(value.length);
    //最多字数限制
    if (len > this.data.max) return;
  },

  /**
   * 上传图片
   */
  upload() {
    let that = this;
    wx.chooseImage({
      count: 5 - that.data.images.length,
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'],
      success(res) {
        res.tempFilePaths.map((item, index) => {
          wx.uploadFile({
            url: that.data.fileUploadUrl, //服务器地址
            filePath: item,//本地照片地址
            name: 'file',
            success(res) {
              const data = JSON.parse(res.data)
              if (data.succeed) {
                that.data.images.push(data.data.id);
                that.setData({ images: that.data.images });
              }
            }
          })
        })
      }
    })
  },

  // 删除图片
  deleteImg(e) {
    const index = e.currentTarget.dataset.index;
    this.data.images.splice(index, 1);
    this.setData({
      images: this.data.images
    });
  },

  /**
   * 图片预览
   */
  previewImage(e) {
    let that = this
    const imageUrl = that.data.fileDownloadUrl + e.currentTarget.dataset.index
    let imageList = []
    if (this.data.images) {
      this.data.images.forEach((item) => {
        imageList.push(that.data.fileDownloadUrl + item)
      })
    }
    wx.previewImage({
      current: imageUrl, // 当前显示图片的http链接
      urls: imageList // 需要预览的图片http链接列表
    })
  },

  // 提交评论
  postComment() {
    let that = this;
    if (!this.data.content) {
      util.showErrorToast('请填写评论')
      return false;
    }
    util.request(api.CommentPost, {
      typeId: that.data.typeId,
      valueId: that.data.valueId,
      orderId: that.data.orderId,
      content: that.data.content,
      images: that.data.images
    }).then(function (res) {
      if (res.succeed) {
        wx.showToast({
          title: '评论成功',
          complete: function () {
            wx.redirectTo({
              url: '/pages/ucenter/order/order',
            })
          }
        })
      }
    });
  },
})