var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');

Page({
  data: {
    orderId: '',
    orderInfo: {},
    orderGoods: [],
    images: [],
    fileUploadUrl: api.FileUploadUrl,
    fileDownloadUrl: api.FileDownloadUrl,
    min: 5,//最少字数
    max: 300, //最多字数 ,
    content: '',
  },
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
    this.setData({
      orderId: options.id
    });
    this.getOrderDetail();
  },
  getOrderDetail() {
    let that = this;
    util.request(api.OrderDetail, {
      orderId: that.data.orderId
    }).then(function (res) {
      if (res.succeed) {
        that.setData({
          orderInfo: res.data.orderInfo,
          orderGoods: res.data.orderGoods,
        });
        //that.payTimer();
      }
    });
  },

  onReady: function () {
    // 页面渲染完成
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },

  //字数限制  
  inputCheck: function (e) {
    // 获取输入框的内容
    var value = e.detail.value;
    this.setData({
      content: value
    });
    // 获取输入框内容的长度
    var len = parseInt(value.length);
    //最多字数限制
    if (len > this.data.max) return;
  },

  /**
   * 上传图片
   */
  upload() {
    let that = this;
    wx.chooseImage({
      count: 3 - that.data.images.length,
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'],
      success(res) {
        res.tempFilePaths.map((item, index) => {
          wx.uploadFile({
            url: that.data.fileUploadUrl, //服务器地址
            filePath: item,//本地照片地址
            name: 'file',
            success(res) {
              const data = JSON.parse(res.data)
              if (data.succeed) {
                that.data.images.push(data.data.id);
                that.setData({ images: that.data.images });
              }
            }
          })
        })
      }
    })
  },

  // 删除图片
  deleteImg(e) {
    const index = e.currentTarget.dataset.index;
    this.data.images.splice(index, 1);
    this.setData({
      images: this.data.images
    });
  },

  /**
   * 图片预览
   */
  previewImage(e) {
    let that = this
    const imageUrl = that.data.fileDownloadUrl + e.currentTarget.dataset.index
    let imageList = []
    if (this.data.images) {
      this.data.images.forEach((item) => {
        imageList.push(that.data.fileDownloadUrl + item)
      })
    }
    wx.previewImage({
      current: imageUrl, // 当前显示图片的http链接
      urls: imageList // 需要预览的图片http链接列表
    })
  },

  // 提交
  postRefund() {
    let that = this;
    if (!this.data.content) {
      util.showErrorToast('请填写退款原因')
      return false;
    }
    util.request(api.OrderRefundApply, {
      orderId: that.data.orderId,
      orderSn: that.data.orderInfo.orderSn,
      amount: that.data.orderInfo.goodsPrice,
      content: that.data.content,
      images: that.data.images
    }).then(function (res) {
      if (res.succeed) {
        wx.showToast({
          title: '提交成功',
          complete: function () {
            wx.redirectTo({
              url: '/pages/ucenter/order/order',
            })
          }
        })
      }
    });
  },
})
