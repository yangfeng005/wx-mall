var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    sts: -1,
    orderList: [],
    page: 1,
    size: 3,
    loadmoreText: '正在加载更多数据',
    nomoreText: '全部加载完成',
    nomore: false,
    totalPages: 1,
    fileDownloadUrl: api.FileDownloadUrl
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.sts) {
      this.setData({
        sts: options.sts
      });
      this.getOrderList(options.sts, 1);
    } else {
      this.getOrderList(-1, 1);
    }
  },

  /**
   * 状态点击事件
   */
  onStsTap: function (e) {
    var sts = e.currentTarget.dataset.sts;
    this.setData({
      sts: sts
    });
    this.getOrderList(sts, 1);
  },

  /**
       * 页面上拉触底事件的处理函数
  */
  onReachBottom: function () {
    this.getOrderList()
  },

  getOrderList(sts, page) {
    wx.showLoading();
    let that = this;

    util.request(api.OrderList, { orderStatus: sts, pageNo: page, pageSize: that.data.size }).then(function (res) {
      if (res.succeed) {
        var orderList = [];
        if (res.additionalProperties.page.pageNo == 1) {
          orderList = res.data;
        } else {
          orderList = that.data.orderList.concat(res.data);
        }
        that.setData({
          orderList: orderList,
          page: res.additionalProperties.page.pageNo,
          totalPages: res.additionalProperties.page.totalPages
        });
      }
      wx.hideLoading();
    });
  },

  payOrder(event) {
    let that = this;
    let orderIndex = event.currentTarget.dataset.orderIndex;
    let order = that.data.orderList[orderIndex];
    wx.redirectTo({
      url: '/pages/pay/pay?orderId=' + order.id + '&actualPrice=' + order.actualPrice,
    })
  },

  comment(event) {
    let that = this;
    let orderIndex = event.currentTarget.dataset.orderIndex;
    let order = that.data.orderList[orderIndex];
    let orderGoodsNumber = order.goodsList.length
    if (orderGoodsNumber == 1) {
      let goods = order.goodsList[0]
      wx.redirectTo({
        url: '/pages/orderComment/orderComment?orderId=' + order.id + '&typeId=0' + '&valueId=' + goods.goodsId,
      })
    } else if (orderGoodsNumber > 1) {
      wx.redirectTo({
        url: '/pages/orderGoods/orderGoods?goodsList=' + JSON.stringify(order.goodsList) + '&orderId=' + order.id,
      })
    }
  },
  //确认收货
  confirmOrder(event) {
    let that = this;
    let orderIndex = event.currentTarget.dataset.orderIndex;
    let orderInfo = that.data.orderList[orderIndex];

    wx.showModal({
      title: '',
      content: '确定已经收到商品？',
      success: function (res) {
        if (res.confirm) {

          util.request(api.OrderConfirm, {
            orderId: orderInfo.id
          }).then(function (res) {
            if (res.succeed) {
              wx.showModal({
                title: '提示',
                content: res.data,
                showCancel: false,
                confirmText: '继续',
                success: function (res) {
                  //  util.redirect('/pages/ucenter/order/order');
                  wx.navigateBack({
                    url: 'pages/ucenter/order/order',
                  });
                }
              });
            }
          });
        }
      }
    });
  },

  //查看物流
  viewDelivery(event) {
    let that = this;
    let orderIndex = event.currentTarget.dataset.orderIndex;
    let orderInfo = that.data.orderList[orderIndex];
    wx.navigateTo({
      url: '/pages/ucenter/shipping/shipping?orderId=' + orderInfo.id,
    });
  },

  //跳转申请退款页面
  refundApply(event) {
    let that = this;
    let orderIndex = event.currentTarget.dataset.orderIndex;
    let orderInfo = that.data.orderList[orderIndex];
    wx.navigateTo({
      url: '/pages/ucenter/refund/refund?id=' + orderInfo.id,
    });
  },

  //取消订单
  cancelOrder(event) {
    let that = this;
    let orderIndex = event.currentTarget.dataset.orderIndex;
    let orderInfo = that.data.orderList[orderIndex];

    wx.showModal({
      title: '',
      content: '确定要取消此订单？',
      success: function (res) {
        if (res.confirm) {

          util.request(api.OrderCancel, {
            orderId: orderInfo.id
          }).then(function (res) {
            if (res.succeed) {
              wx.showModal({
                title: '提示',
                content: res.data,
                showCancel: false,
                confirmText: '继续',
                success: function (res) {
                  //  util.redirect('/pages/ucenter/order/order');
                  wx.navigateBack({
                    url: 'pages/ucenter/order/order',
                  });
                }
              });
            }
          });
        }
      }
    });
  },

  toOrderDetailPage(event) {
    let that = this;
    let orderIndex = event.currentTarget.dataset.orderIndex;
    let orderInfo = that.data.orderList[orderIndex];
    wx.navigateTo({
      url: '/pages/ucenter/orderDetail/orderDetail?id=' + orderInfo.id,
    });
  },


  onReady: function () {
    // 页面渲染完成
  },
  onShow: function () {

  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
  /**
  * 页面上拉触底事件的处理函数
  */
  onReachBottom: function () {
    if (this.data.page < this.data.totalPages) {
      this.getOrderList(this.data.sts, this.data.page + 1);
    }
  }
})
