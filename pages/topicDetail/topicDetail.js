var app = getApp();
var WxParse = require('../../lib/wxParse/wxParse.js');
var util = require('../../utils/util.js');
var api = require('../../config/api.js');

Page({
  data: {
    id: 0,
    topic: {},
    topicList: [],
    commentCount: 0,
    commentList: []
  },onShareAppMessage: function() {//自定义函数 用户点击右上角分享给好友,要先在分享好友这里设置menus的两个参数,才可以分享朋友圈 
    wx.showShareMenu({ 
        withShareTicket: true, 
        menus: ['shareAppMessage', 'shareTimeline'] 
      }) 
    return { 
        title: this.data.topic.title, 
        desc: this.data.topic.content, 
        imageUrl: this.data.topic.scene_pic_url, 
        path: '/pages/topicDetail/topicDetail?id='+this.data.id 
    } 
  },onShareTimeline: function () {//用户点击右上角分享朋友圈 
  return { 
      title: this.data.topic.title, 
      query: { 
        id:this.data.id 
      }, 
      imageUrl: this.data.topic.picUrl 
    } 
  },onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
    var that = this;
    that.setData({
      id: options.id
    });
    util.request(api.TopicDetail, { id: that.data.id}).then(function (res) {
      if (res.succeed) {
        that.setData({
          topic: res.data,
        });
        WxParse.wxParse('topicDetail', 'html', res.data.content, that);
      }
    });
  },
  
  onReady: function () {

  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏

  },
  onUnload: function () {
    // 页面关闭

  },

  //富文本a标签跳转
  wxParseTagATap: function (e) {
    var href = e.currentTarget.dataset.src;
    let url = href.join("")
    //我们可以在这里进行一些路由处理
    if (href) {
      wx.redirectTo({
        url: url
      })
    }
  }
})